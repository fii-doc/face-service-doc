/**
 * @apiName AccountHistory
 * @api {get} /history/account Operations
 * @apiDescription Get the activities of this system of a account
 * @apiGroup History
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [sort] Sort result acccording to account_id, action, or time
 * @apiParam {Number} [accountId] The unique id of the account.
 * @apiParam {Number} [action]  Type of the action.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "list": [
 *             {
 *                 "id": 322,
 *      	       "accountId": 2
 *      	       "action": 2
 *                 "target": "4",
 *                 "detail": "account modification",
 *      	       "timeCreated": "2019-10-30T12:43:11Z"
 *      	   },
 *      	   {
 *                 "id": 323,
 *      	       "accountId": 2
 *                 "action": 5
 *                 "target": "G0200220",
 *                 "detail": "delete person",
 *                 "timeCreated": "2019-10-30T12:43:11Z"
 *              }
 *          ]
 *      }
 */




 /**
 * @apiName RecognitionHistory
 * @api {get} /history/recognition Recognitions
 * @apiDescription recognition
 * @apiGroup History
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [sort] Sort result acccording to personId, personIdDesc, faceId, headTemp, headTempDesc, time, timeDesc. Default by time.
 * @apiParam {Number} [personId] The unique id of the person.
 * @apiParam {Number} [faceId] The unique id of the face.
 * @apiParam {Number} [accountId] The unique id of the account.
 * @apiParam {Number} [headTemp]  Head temperature. 0: all, 1: only has headTemp, 2: only no headTemp. default is 0
 * @apiParam {Number} [pageSize] Page size fo the result. Deafult is no limited.
 * @apiParam {Number} [pageNumber] The page number.
 *
 * @apiSuccessExample Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "status": 0,
 *          "message": "ok",
 *          "pageNumber": 1,
 *          "pageSize": 10,
 *          "historyCount": 233,
 *          "list": [
 *              {
 *                  "id": 1322,
 *                  "personId": 2,
 *                  "faceId": 2,
 *                  "oriFaceUrl": "",   // the download url of the picture registered in face database.
 *                  "faceUrl": "", // the download url of the picture to be recognized.
 *                  "findFaceTime": 27.5381,
 *                  "encodeFaceTime": 161.256,
 *                  "recognizeTime": 0.118732,
 *                  "payload": {
 *                      "headTemp": 36.7
 *                  },
 *                  "timeCreated": "2019-10-30T12:43:11Z"
 *              },
 *              {
 *                  "id": 1529,
 *                  "personId": 2,
 *                  "faceId": 2,
 *                  "faceUrl": "",
 *                  "findFaceTime": 27.5381,
 *                  "encodeFaceTime": 161.256,
 *                  "recognizeTime": 0.118732,
 *                  "payload": {},
 *                  "timeCreated": "2019-10-31T12:43:11Z"
 *              },
 *              {
 *                  "id": 1744,
 *                  "personId": "--08993699715876469050971",
 *                  "faceId": -7,
 *                  "oriFaceUrl": "/face/image/-7",
 *                  "faceUrl": "/history/recognition/image/51",
 *                  "findFaceTime": 27.5381,
 *                  "encodeFaceTime": 161.256,
 *                  "recognizeTime": 0.118732,
 *                  "payload": {},
 *                  "timeCreated": "2020-04-23T13:01:47.810061+00:00"
 *              }
 *          ]
 *      }
 */
