/**
 * @apiDefine BearerToken
 *
 * @apiHeader {String} authorization Bearer JWT Authorization token
 */

 /**
 * @apiDefine ContentTypeJson
 * @apiHeader {String} Content-Type  should be 'application/json'
 */

/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *         "error": "UserNotFound"
 *     }
 */

/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup Face
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "firstname": "John",
 *         "lastname": "Doe"
 *     }
 *
 * @apiUse UserNotFoundError
 */

/**
 * @api {get} /user/:id Request User information
 * @apiName GetUser
 * @apiGroup Person
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "firstname": "John",
 *         "lastname": "Doe"
 *     }
 *
 * @apiUse UserNotFoundError
 */
