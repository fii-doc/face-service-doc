 /**
* @apiName Register
* @api {POST} /face[?type=TYPE] Register
* @apiDescription Register a face by uploading a picture with a single face.
* @apiGroup Face
*
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
*
* @apiParam {String} [type] Upload image type. Should be file or base64.
* @apiParam {String} personId The unique id of the person.
* @apiParam {File/String} image According to type parameter. file: PNG is recommanded. base64:  base64 encoded string of image file, starts with image metadata, for example: "data:image/jpeg;base64,"
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 201 Created
*     {
*         "status": 0,
*         "message": "ok",
*         "id": 14438729
*     }
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 400 Bad Request
*     {
*         "status": 30001,
*         "message": "No face found."
*     }
*
*     HTTP/1.1 400 Bad Request
*     {
*         "status": 30002,
*         "message": "More than one face found in the source image"
*     }
*/

 /**
 * @apiName Delete
 * @api {DELETE} /face/:faceId Delete
 * @apiDescription Delete a face
 * @apiGroup Face
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} faceId The unique id of a face.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 202 Accepted
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 30003,
 *         "message": "The face id does not exist."
 *     }
 */

  /**
 * @apiName List
 * @api {GET} /face[?sort=SORT&personId=PERSON_ID] List
 * @apiDescription Get a face list
 * @apiGroup Face
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [sort] Sort result acccording to personId, faceId, time, timeDesc. Default by time.
 * @apiParam {Number} [personId] The unique id of the person.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "list": [
 *             {
 *                 "faceId":1,
 *                 "accountId": 1,
 *                 "personId": "G0002201"
 *                 "oriFaceUrl": "/face/image/1",
 *                 "timeCreated": "2019-10-30T12:43:11Z"
 *             },
 *             {
 *                 "faceId":4,
 *                 "accountId": 1,
 *                 "personId": "G0002203"
 *                 "oriFaceUrl": "/face/image/4",
 *                 "timeCreated": "2019-10-30T12:43:11Z"
 *             }
 *         ]
 *     }
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 20002,
 *         "message": "The person id does not exist."
 *     }
 */

  /**
 * @apiName Detect
 * @api {get} /face/detect[?type=TYPE] Detect
 * @apiDescription Find all the faces that appear in a picture.
 * @apiGroup Face
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [type] Upload image type. Should be file or base64.
 * @apiParam {File/String} image According to type parameter. file: PNG is recommanded. base64:  base64 encoded string of image file, starts with image metadata, for example: "data:image/jpeg;base64,"
 * @apiParam {Nimber} [detectMask] Set to 1 to enable mask detection, default is 0.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "faceCount": 2,
 *         "faces": [
 *             {
 *                 "signature": "<BASE_64_ENCODED>",
 *                 "location": {
 *                     "top": 167,
 *                     "right": 190,
 *                     "bottom": 250,
 *                     "left": 250,
 *                     "mask": 1
 *                 }
 *             },
 *             {
 *                 "signature": "<BASE_64_ENCODED>",
 *                 "location": {
 *                     "top": 157,
 *                     "right": 390,
 *                     "bottom": 210,
 *                     "left": 236,
 *                     "mask": 0
 *                 }
 *             }
 *         ]
 *     }
 */

  /**
 * @apiName Recognize
 * @api {get} /face/recognize[?type=TYPE] Recognize
 * @apiDescription Recognize all the faces that appear in a picture.
 * @apiGroup Face
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [type] Upload image type. Should be file or base64.
 * @apiParam {File/String} image According to type parameter. file: PNG is recommanded. base64:  base64 encoded string of image file, starts with image metadata, for example: "data:image/jpeg;base64,"
 * @apiParam {String} dataType Determine what format the result picture presented by. The value is url, base64

 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "faceCount": 2,
 *         "faces": [
 *             {
 *                 "personId": "830850",
 *                 "faceId": 155723,
 *                 "oriFaceUrl": "/face/image/155723",
 *                 "oriFaceData": "<BASE_64_ENCODED>"
 *                 "location": {
 *                     "top": 167,
 *                     "right": 390,
 *                     "bottom": 322,
 *                     "left": 236
 *                 },
 *                 "historyId": 1902
 *              },
 *              { // stranger
 *                  "personId": "--08993699715876469050971",
 *                  "faceId": -1,
 *                  "location": {
 *                      "top": 167,
 *                      "right": 390,
 *                      "bottom": 322,
 *                      "left": 236
 *                  },
 *                  "historyId": 1903
 *              }
 *         ]
 *     }
 */

  /**
 * @apiName Compare
 * @api {get} /face/compare Compare signature
 * @apiDescription Compare the given faces signature with face database.
 * @apiGroup Face
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {File} image Only aupport jps file.
 * @apiParam {String} dataType Determine what format the result picture presented by. The value is url, base64
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "faceCount": 2,
 *         "faces": [
 *             {
 *                 "personId": "830850",
 *                 "faceId": 155723,
 *                 "oriFaceUrl": "/face/image/155723",
 *                 "oriFaceData": "<BASE_64_ENCODED>"
 *                 "location": {
 *                     "top": 167,
 *                     "right": 390,
 *                     "bottom": 322,
 *                     "left": 236
 *                 },
 *                 "historyId": 1902
 *             },
 *             { // stranger
 *                 "personId": "--08993699715876469050971",
 *                 "faceId": -1,
 *                 "location": {
 *                     "top": 167,
 *                     "right": 390,
 *                     "bottom": 322,
 *                     "left": 236
 *                 },
 *                 "historyId": 1903
 *             }
 *         ]
 *     }
 */