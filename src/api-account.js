/**
 * @apiName Create
 * @api {POST} /account Create
 * @apiDescription Register an account
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} username Username Username of the account.
 * @apiParam {String} password Password Username of the account.
 * @apiParam {String} [firstName] Firstname of the user.
 * @apiParam {String} [lastName] Lastname of the user.
 * @apiParam {String} [mobile] Phone number of the user.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "id": 2
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10001,
 *         "message": "The account name already exists."
 *     }
 */


/**
 * @apiName Login
 * @api {POST} /account/login Login
 * @apiDescription Authenticate an account in Face Recognition Service. The issuer of the One Time Password will dictate if a JWT Token may be issued in the API response. Current the JWT's expiration duration is set to `1 day` for testing
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 *
 * @apiParam {String} Username Username of the account.
 * @apiParam {String} Password Password of the account.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "id": 2,
 *         "access_token": "eyJ0eXAiOiJKV1QiLCJh......."
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *         "status": 10002,
 *         "message": "the account does not exist or password is wrong"
 *     }
 *
 *     HTTP/1.1 403 Forbidden
 *     {
 *         "status": 10004,
 *         "message": "the account is locked or disabled"
 *     }
 */

 /**
 * @apiName Update
 * @api {PUT} /account/:accountId Update
 * @apiDescription Update account profile
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} accountId The unique id of the account.
 * @apiParam {String} [firstName] Firstname of the user.
 * @apiParam {String} [lastName] Lastname of the user.
 * @apiParam {String} [mobile] Phone number of the user.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10002,
 *         "message": "The account id does not exist."
 *     }
 */

 /**
 * @apiName UpdateRole
 * @api {PUT} /account/role/:accountId UpdateRole
 * @apiDescription Change the role of the account. ONLY used by super account.
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} accountId The unique id of the account.
 * @apiParam {Number} role The role of the account. 0: admin, 1: user
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10002,
 *         "message": "The account id does not exist."
 *     }
 *
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10003,
 *         "message": "Only super account can perform this action."
 *     }
 */

 /**
 * @apiName ChangePassword
 * @api {PUT} /account/password ChangePassword
 * @apiDescription Change account password. The super account can change the password of all accounts without confirming existing password.
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} accountId Unique id of the Account.
 * @apiParam {String} currentPassword Current password for the account.
 * @apiParam {String} newPassword New password of the account.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10005,
 *         "message": "current password does not match"
 *     }
 *
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10002,
 *         "message": "The account id does not exist."
 *     }
 */

 /**
 * @apiName Delete
 * @api {DELETE} /account/:accountId Delete
 * @apiDescription Delete a account. Only used for super account.
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [accountId] The unique id of the account.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 202 Accept
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 10002,
 *         "message": "The account id does not exist."
 *     }
 */

  /**
 * @apiName List
 * @api {GET} /account List
 * @apiDescription Get account list.
 * @apiGroup Account
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [sort] Sort result acccording to accountId, username, role, firstName, lastName, mobile, provider, time, or timeDesc.
 * @apiParam {Number} [role] Role of the account.
 * @apiParam {Number} [provider]  Type of providers from account.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "list": [
 *             {
 *                 "accountId": 45,
 *                 "username": "trevor",
 *                 "role": 1,
 *                 "firstName": "Trevor",
 *                 "lastName": "Philips"
 *                 "mobile": "273-555-0136",
 *                 "provider": 0,
 *                 "timeCreated": "2019-10-30T22:12:57Z"
 *             },
 *             {
 *                 "accountId": 1,
 *                 "username": "franklin",
 *                 "role": 1,
 *                 "firstName": "Franklin",
 *                 "lastName": "Cliton"
 *                 "mobile": "328-555-0156",
 *                 "provider": 0,
 *                 "timeCreated": "2019-10-30T22:12:57Z"
 *             }
 *         ]
 *     }
 */