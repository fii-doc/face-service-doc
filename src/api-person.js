/**
 * @apiName Register
 * @api {POST} /person/ Register
 * @apiDescription Register a person, that can be used as en employee, student, or any identity of a organization
 * @apiGroup Person
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} personId The Unique id of the person.
 * @apiParam {String} name The name of the person.
 * @apiParam {Number} position  Postition of the grade.
 * @apiParam {String} comment Comment.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 201 Created
 *    {
 *        "status": 0,
 *        "message": "ok",
 *    }
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 400 Bad Request
 *    {
 *        "status": 20001,
 *        "message": "The person id already exists."
 *    }
 *
 */

 /**
 * @apiName Update
 * @api {PUT} /person/:personId Update
 * @apiDescription Update profile of a person
 * @apiGroup Person
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} personId The Unique id of the person.
 * @apiParam {String} name The name of the person.
 * @apiParam {Number} position  Postition of the grade.
 * @apiParam {String} comment Comment.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 204 No Content
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 400 Bad Request
 *    {
 *        "status": 20002,
 *        "message": "The person id does not exist."
 *    }
 */

 /**
 * @apiName Get info
 * @api {get} /history/account getinfo
 * @apiDescription Get a person, admin account can get a specific person information, user account can only get his person information.
 * @apiGroup Person
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} personId The Unique id of the person.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "name": "Ben",
 *         "position": 3,
 *         "comment": "Employee of Foxconn Inc.",
 *         "timeCreated": "2019-10-30T12:43:11Z"
 *     }
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 20002,
 *         "message": "The person id does not exist."
 *     }
 */

 /**
 * @apiName List
 * @api {GET} /history/account List
 * @apiDescription Get a person list.
 * @apiGroup Person
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} [sort] Sort result acccording to personId, time, timeDesc, position, or name. Default by time.
 * @apiParam {String} [personId] The unique id of the person.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 0,
 *         "message": "ok",
 *         "list": [
 *             {
 *                 "personId": "G0002201"
 *                 "name": "Ben",
 *                 "position": 3,
 *                 "comment": "Employee of Foxconn Inc.",
 *                 "timeCreated": "2019-10-30T12:43:11Z"
 *             },
 *             {
 *                 "personId": "G2004409"
 *                 "name": "Terry",
 *                 "position": 0,
 *                 "comment": "President of Foxconn Inc.",
 *                 "timeCreated": "2019-10-30T12:43:11Z"
 *             }
 *         ]
 *     }
 */

 /**
 * @apiName Delete
 * @api {DELETE} /person/:personId Delete
 * @apiDescription Delete a person
 * @apiGroup Person
 *
 * @apiUse ContentTypeJson
 * @apiUse BearerToken
 *
 * @apiParam {String} personId The Unique id of the person.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 202 Accept
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *         "status": 20002,
 *         "message": "The person id does not exist."
 *     }
 */
